from flask import Flask, request, jsonify
from flask_sqlalchemy import SQLAlchemy
import os


app = Flask(__name__)


if __name__ == '__main__':
    app.run(debug=True)


app.config['SQLALCHEMY_DATABASE_URI'] = os.environ.get('DATABASE_URL')
db = SQLAlchemy(app)


class Company(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    company_name = db.Column(db.String(200), unique=True, nullable=False)
    trading_name = db.Column(db.String(150), unique=True, nullable=False)
    tax_code = db.Column(db.String(20), unique=True, nullable=False)
    issue_date = db.Column(db.DateTime, nullable=False)
    status = db.Column(db.String(50), nullable=False)
    issuing_authority = db.Column(db.String(100), nullable=False)
    address = db.Column(db.String(200), nullable=False)
    business_owner = db.Column(db.String(100), nullable=False)
    ceo = db.Column(db.String(100), nullable=True)
    business_sector = db.Column(db.String(50), nullable=True)
    notes = db.Column(db.String(500), nullable=True)

    def __init__(self, company_name, trading_name, tax_code, issue_date, status, issuing_authority, address, business_owner, ceo, business_sector, notes):
        self.company_name = company_name
        self.trading_name = trading_name
        self.tax_code = tax_code
        self.issue_date = issue_date
        self.status = status
        self.issuing_authority = issuing_authority
        self.address = address
        self.business_owner = business_owner
        self.ceo = ceo
        self.business_sector = business_sector
        self.notes = notes


db.create_all()


# CRUD operations
@app.route('/company/<id>', methods=['GET'])
# Get a single company
def get_company(id: int):
    company = Company.query.get(id)
    del company.__dict__['_sa_instance_state']
    return jsonify(company.__dict__)


# Get all company
@app.route('/company', methods=['GET'])
def get_companies():
    companies = []
    for company in db.session.query(Company).all():
        del company.__dict__['_sa_instance_state']
        companies.append(company.__dict__)
    return jsonify(companies)


# Create a new company
@app.route('/company', methods=['POST'])
def create_company():
    body = request.get_json()
    db.session.add(Company(body["company_name"], body["trading_name"], body["tax_code"], body["issue_date"], body["status"],
                   body["issuing_authority"], body["address"], body["business_owner"], body["ceo"], body["business_sector"], body["notes"]))
    db.session.commit()
    return jsonify({"message": "Company created successfully"})


# Update a company
@app.route('/company/<id>', methods=['PUT'])
def update_company(id: int):
    body = request.get_json()
    db.session.query(Company).filter_by(id=id).update(
        dict(company_name=body["company_name"], trading_name=body["trading_name"], tax_code=body["tax_code"], issue_date=body["issue_date"],
             status=body["status"], issuing_authority=body["issuing_authority"], address=body["address"], business_owner=body["business_owner"],
             ceo=body["ceo"], business_sector=body["business_sector"], notes=body["notes"]))
    db.session.commit()
    return jsonify({"message": "Company updated successfully"})


# Delete a company
@app.route('/company/<id>', methods=['DELETE'])
def delete_company(id: int):
    db.session.query(Company).filter_by(id=id).delete()
    db.session.commit()
    return jsonify({"message": "Company deleted successfully"})
